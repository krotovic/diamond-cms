<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */
namespace Diamond\Packages;
use Diamond\App\DiamondException;
use Diamond\Application;

/**
 * Class Package
 * @package Diamond\Packages
 */
class Package {

    protected
    $app,
    $engine;

    /**
     * Package itself is publicly accessible.
     * @var \Diamond\Packages\Package
    */
    public $package;

    /**
     * Config variables for this package.
     * @var array
     */
    public $config;

    /**
     * Indicates if package is enabled and registered.
     * @var bool
     */
    protected
    $enabled = false,
    $registered = false;

    /**
     * Constructor for Packages to install and register them.
     * @param Package $package
     * @param array $config
     * @link http://getdiamond.cz/docs/Diamond/Packages
     */
    public function __construct(Package $package, array $config = array()){
        $this->package = $package;
        $this->config = $config;
        $this->app = Application::getInstance();
        $this->engine = $this->app->get("engine");
    }

    /**
     * Install package to application and database.
     *
     * @return void
     * @throws \Diamond\App\DiamondException
     */
    protected function install(){

    }

    /**
     * Register package to package provider.
     *
     * @return \Diamond\Packages\Package
     * @throws \Diamond\App\DiamondException
     */
    protected function register(){
        if($this->package){
            $this->registered = true;
            $this->app->registerPackage($this->package);
        } else {
            throw new DiamondException("Packages wasn't loaded correctly.");
        }

        return $this->package;
    }

    /**
     * Render result of package layout.
     *
     * @return mixed
     */
    public function render() {

    }

}