<?php
namespace Diamond\App;

/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */

/**
 * Class Loader
 * @package Diamond\App
 */
class Loader {

    protected
    $loaded = array(),
    $engine = null,
    $controller = null,
    $router = null;

    protected $config;

    public function __construct(){
    }

    /**
     * Get layout engine.
     * @return Layout
     */
    public function getEngine(){
        return $this->engine;
    }

    /**
     * Get controller class.
     * @return Controller
     */
    public function getController(){
        return $this->controller;
    }

    /**
     * Get router.
     * @return Router
     */
    public function getRouter(){
        return $this->router;
    }

    public function loadAll() {
        $this->router = new Router();
        $this->controller = new Controller();
        $this->engine = new Layout();
    }

    public function load($class) {
        $tmp = null;
        if (file_exists($class)){
            if (class_exists($class)){
                $tmp = eval("new ".$class."()");
            }
            else throw new DiamondException("Couldn't load class: ".$class);
        } else throw new DiamondException("File doesn't exist: ".$class);

        return false;
    }

    public function loadClass($class) {
        return $this->load($class);
    }

} 