<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */
namespace Diamond\App;
use Exception;

/**
 * Class DiamondException
 * @package Diamond\App\Exception
 */
class DiamondException extends Exception {

    protected
    $message = "Unknown Exception",
    $code = 0,
    $file,
    $line;

    /**
     * @param string $message
     * @param int $code
     */
    public function __construct($message = null, $code = 0){
        parent::__construct($message, $code);
    }

    /**
     * Custom message to show from Exception.
     * @return string
     */
    public function __toString(){
        return "Diamond caught this {$this->message} in file {$this->file} on line {$this->line}";
    }

}