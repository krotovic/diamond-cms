<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */
namespace Diamond\App;

/**
 * Base Controller.
 *
 * Class Controller
 * @package Diamond\App
 */
class Controller {

    protected $loader;

    public function __construct(){
        if(!$this->beforeLoad()){
            error_log(__CLASS__."'s beforeLoad method returned error.");
        }
    }
    /**
     * Tells Loader what to do before load this class.
     *
     * @return bool
     */
    public function beforeLoad(){
        return true;
    }

    public function afterRender($result){
        return $result;
    }
}