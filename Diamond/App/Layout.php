<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */
namespace Diamond\App;

/**
 * Class Layout
 * @package Diamond\App
 */
class Layout {

    protected
        $renderLayers = array(),
        $header = '<!DOCTYPE html><html><head>',
        $footer = '</body></html>';

    /**
     * This function will render the requested template with parsed variables and returns a string.
     *
     * @param string $layout
     * @param string $context
     * @param array|null $variables
     * @return string
     */
    public function render($layout, $context = "", $variables = null) {
        $result = "" .
        /* Add header to rendered result. */
        $this->header .
        /* Now add meta and title to result. */
            $this->getMeta() .
        $this->renderLayers();

        return $result;
    }

    /**
     * This function renders all layers stored in Layout.
     * @return string
     */
    private function renderLayers() {
        return "";
    }

    /**
     * This function is key for rendering, it parses string and variables together and returns complete html string.
     * @param string $layout
     * @param array $vars
     * @return string
     */
    private function parse($layout, $vars) {
        return "";
    }

    private function getMeta()
    {
        return "";
    }

} 