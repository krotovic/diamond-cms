<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */
namespace Diamond\App;
use Diamond\Application;

/**
 * Class Database
 * @package Diamond\App
 */
class Database {

    private
    $credentials,
    $database;

    public function __construct() {
        $this->config = $config = Application::getInstance()->get("config");
        $dsn = $config["db"]["driver"] . ":dbname=" . $config["db"]["database"] . ";host=" . $config["db"]["host"];
        try {
            $this->pdo = new \PDO($dsn, $config["db"]["user"], $config["db"]["pass"]);
        } catch (\PDOException $e) {
            throw new DiamondException(__CLASS__." couldn't establish database connection. Please, validate credentials in config.php file.");
        }
    }

    public function getRow ($table, $vars) {
        if (is_array($vars) && count($vars)) throw new DiamondException(__FUNCTION__." need to get $vars as not-empty array.");
        if (is_string($vars) && $vars == "") throw new DiamondException(__FUNCTION__." need to get specific variable for this table to obtain one row.");
        $statement = "SELECT * FROM ".$table." WHERE ";
        if (is_array($vars)) {

        }
        return $this->pdo;
    }


} 