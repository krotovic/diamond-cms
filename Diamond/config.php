<?php

return array(

    /**
     * Environment
     * -------------------
     *
     * There you can set your environment
     * and with this you can use custom
     * modules.
     *
     * For example, if you'd like use Developer Tools,
     * you should set app's environment to "development".
     *
     * Available environment values:
     *      development => For developing purpose.
     *      production => /Default/ For production.
     *      testing => For testing your app.
     *
     */
    "environment" => "development",

    /**
     * Error reporting
     * ---------------------
     *
     * You can choose where should
     * be stored all errors.
     *
     * You can choose from direct mails or
     * reporting to one file.
     *
     * For mails set value to "mail",
     * for file (as default) set to "log_file".
     */
    "error_reporting" => "log_file",
    "log_file" => "default.log",

    "db" => array(
        "host" => "localhost",
        "database" => "DB_NAME",
        "user" => "DB_USER",
        "pass" => "DB_PASSWORD"
    )

);