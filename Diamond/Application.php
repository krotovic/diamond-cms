<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */

namespace Diamond;
use Diamond\App;
use Diamond\App\Error\ErrorHandler as Error;

/**
 * Diamond application provider and container.
 *
 * Class Application
 * @package Diamond
 */
class Application {

    public static $app;
    public
    $config,
    $vendor,
    $loader
    ;

    protected
    $layers = array();

    public function __construct(){
        self::$app = $this;
        $this->loader = new App\Loader();
        $this->loader->loadAll();
    }

    /**
     * Create application.
     *
     * @return void
     */
    public function run(){

    }

    public function get($item) {
        switch ($item) {
            case "engine": return $this->loader->getEngine();
            case "controller": return $this->loader->getController();
            case "router": return $this->loader->getRouter();
            case "config": return $this->config;
            default: return false;
        }
    }

    public static function getInstance() {
        return self::$app;
    }

    /**
     * Set global configuration.
     *
     * @param $config
     * @return void
     */
    public function setConfig($config){
        $this->config = $config;
    }

    /**
     * Load class to the app.
     *
     * @param $class
     * @return bool
     */
    public function loadClass($class){
        return $this->loader->loadClass($class);
    }

    /**
     * Set $vendor variable.
     *
     * @param $vendor
     * @return void
     */
    public function setVendor($vendor){
        $this->vendor = $vendor;
    }

} 