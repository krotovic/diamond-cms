<?php

spl_autoload_register(function ($className) {
    $ds = DIRECTORY_SEPARATOR;
    $dir = APP_PATH;
    $className = str_replace('\\', $ds, $className);
    $file = "{$dir}{$className}.php";
    if (is_readable($file) && file_exists($file)) /** @var $file string */
        include $file;
});