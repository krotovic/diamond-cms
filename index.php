<?php
/**
 * @author Lukáš Krotovič <lukas@krotovic.cz>
 */

define("APP_PATH", __DIR__ . DIRECTORY_SEPARATOR);
define("DIAMOND", __DIR__ . "/Diamond" . DIRECTORY_SEPARATOR);

include_once DIAMOND."autoload.php";
$config = include_once DIAMOND . "config.php";
$vendor = (file_exists(APP_PATH . "vendor/autoload.php"))? include APP_PATH . "vendor/autoload.php": null;

$app = new Diamond\Application();
global $app;

if (isset($config['environment'])){
    if($config['environment'] === "production") error_reporting(0);
    else error_reporting(E_ALL);
}

$app->setConfig($config);
$app->setVendor($vendor);
$app->run();