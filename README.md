# Diamond System (work-in-progress)
Diamond System is lightweight and powerful content-management-system based on custom framework and modules system.

###The pre-alpha version is *in progress*.

If you [email me](mailto:lukas@krotovic.cz?Subject=DIAMOND) I can tell you when it's ready.

Main features will be:

* Fully powerful, easy administration
* Graphic designer free version available on package-store
* You can use this system even for your app just like startup framework
* Totally free! But Deluxe version available on the site [http://getdiamond.cz](http://getdiamond.cz) for a little fee.

## Installation
### Requirements
This system requires only PHP version 4 and higher.

### Install process
To install this content management system you need just download this whole repo
(there or on our website: [getdiamond.cz](http://getdiamond.cz) - *not available right now*)
and unzip downloaded zip-file into your root folder on your hosting and run it in your browser.
You will be guided through installation process.
Everything else you can manage with administration and custom packages.

## Package-based system
This system is unique because it can't do anything without any package :D.
So every copy of this system needs some packages installed.
Within installation process there will be some basic packages installed into your system.
The Diamond System will be completely yours. You can create your own system just with installing custom packages.
This system differs from others (like WordPress, Joomla, etc.) in one important point.
All those systems are mainly focused on blog-style websites. We are not.
This system will give you an advanced option to create your own easy-editable website or any application for your business.
The idea of this system is:
> Your content and website, your system.


## Contributing
This project is developed under Apache license version 2.0. If you'd like to help with development of this project,
fork this project and give me report about your changes to my [email](mailto:lukas@krotovic.cz).

Or you can create custom packages after pre-alpha release is ready. I will provide later some more details about community development.


### 'Hope you enjoy!